//
//  BungieApi.swift
//  Sidearm
//
//  Created by Jonathan Alexander on 2/13/24.
//

import Foundation
import SwiftData

@Model
class BungieApi {
    private let API_KEY = "fc2cb7e8db564d2cb20c7975d31c0966"
    private let CLIENT_ID = "46295"
    private let CLIENT_SECRET = "X2PRtlEwO2666ezvmFUQrbQiJzh0Mu2yHWwHmP3N5.M"
    
    private let API_BASE_URL = URL(string: "https://www.bungie.net/Platform/")
    
    private var authCodes: AuthCodes?
    private var refreshCodes: AuthCodes?
    
    private var manifestVersion: String?
    @Attribute(.externalStorage) private var manifestCache: Data?
    
    var bnetMembershipId: String?
    var membershipId: String?
    var membershipType: BungieMembershipType?
    
    init() {}
    init(refreshCodes: AuthCodes, manifestVersion: String, manifestCache: Data, bnetMembershipId: String, membershipId: String, membershipType: BungieMembershipType) {
        self.refreshCodes = refreshCodes
        self.manifestVersion = manifestVersion
        self.manifestCache = manifestCache
        self.bnetMembershipId = bnetMembershipId
        self.membershipId = membershipId
        self.membershipType = membershipType
    }
    
    func checkNewManifest() async throws -> (String, String)? {
        guard let manifestVersionUrl = URL(string: "Destiny2/Manifest/", relativeTo: self.API_BASE_URL) else {
            throw BungieApiError.urlBuildFailed
        }
        
        let resp: DestinyManifest = try await self.authApiGet(endpoint: manifestVersionUrl)
        
        if (manifestVersion != resp.version) {
            guard let newManifestPath = resp.jsonWorldContentPaths["en"] else {
                throw BungieApiError.paramMissingFromResp
            }
            return (newManifestPath, resp.version)
        } else {
            return nil
        }
    }
    
    func getNewManifest(path: String, version: String) async throws -> Data {
        guard let newManifestUrl = URL(string: path, relativeTo: self.API_BASE_URL) else {
            throw BungieApiError.urlBuildFailed
        }
        
        var req = URLRequest(url: newManifestUrl)
        req.httpMethod = "GET"
        
        let (data, _) = try await URLSession.shared.data(for: req)
        
        self.manifestCache = data
        self.manifestVersion = version
        
        return data
    }
    
    func getManifestCache() -> Data? {
        return manifestCache
    }
        
    func isLoggedIn() -> Bool {
        return self.authCodes != nil && self.refreshCodes != nil
    }
    
    func getLoginUrl() -> URL {
        return (URL(string: "https://www.bungie.net/en/oauth/authorize")?.appending(queryItems: [URLQueryItem(name: "response_type", value: "code"), URLQueryItem(name: "client_id", value: self.CLIENT_ID)]))!
    }
    
    private func apiGet<T: DestinyResponse>(endpoint: URL) async throws -> T {
        return try await self.apiGet<T>(endpoint: endpoint, headers: [:])
    }
    
    private func apiGet<T: DestinyResponse>(endpoint: URL, headers: [String: String]) async throws -> T {
        var request = URLRequest(url: endpoint)
        request.httpMethod = "GET"
        for (key, value) in headers {
            request.addValue(value, forHTTPHeaderField: key)
        }
        
        let (data, _) = try await URLSession.shared.data(for: request)
        var resp: BungieResp<T>
        do {
            resp = try JSONDecoder().decode(BungieResp<T>.self, from: data)
        } catch {
            print(String(data: data, encoding: .utf8) ?? "")
            print(error)
            throw error
        }
        
        guard let response = resp.Response else {
            print("endpoint: \(endpoint)")
            print("resp: \(resp)")
            throw BungieApiError.bungieApiError(errorMsg: resp.ErrorStatus)
        }
        
        return response
    }
    
    private func authApiGet<T: DestinyResponse>(endpoint: URL) async throws -> T {
        if (self.authCodes?.expires ?? Date(timeIntervalSince1970: 0) < Date()) {
            try await self.refreshLogin()
        }
        
        guard let authCodes = self.authCodes else {
            throw BungieApiError.authTokenMissing
        }
        
        return try await self.apiGet(endpoint: endpoint, headers: ["Authorization": "Bearer \(authCodes.token)", "X-API-Key": self.API_KEY])
    }
    
    private func refreshLogin() async throws {
        guard let refreshCodes = self.refreshCodes else {
            throw BungieApiError.refreshWithoutRefreshToken
        }
        
        guard refreshCodes.expires > Date() else {
            throw BungieApiError.refreshTokenExpired
        }
        
        await self.tokenLogin(code: refreshCodes.token, isRefresh: true)
    }
    
    func tokenLogin(code: String, isRefresh: Bool = false) async {
        guard let authUrl = URL(string: "https://www.bungie.net/platform/app/oauth/token/") else {
            print("Failed to build token URL, this should never happen!")
            return
        }
        var request = URLRequest(url: authUrl)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let body = "client_id=\(self.CLIENT_ID)&client_secret=\(self.CLIENT_SECRET)"
        let grantType = isRefresh ? "refresh_token" : "authorization_code"
        let codeKey = isRefresh ? "refresh_token" : "code"
        request.httpBody = "\(body)&grant_type=\(grantType)&\(codeKey)=\(code)".data(using: .utf8)
        
        do {
            let (data, _) = try await URLSession.shared.data(for: request)
            let result = try JSONDecoder().decode(BungieTokenResp.self, from: data)
            self.authCodes = AuthCodes(token: result.access_token, expires: Date().addingTimeInterval(TimeInterval(result.expires_in)))
            self.refreshCodes = AuthCodes(token: result.refresh_token, expires: Date().addingTimeInterval(TimeInterval(result.refresh_expires_in)))
            self.bnetMembershipId = result.membership_id
        } catch {
            print("Token request failed: \(error)")
        }
    }
    
    func checkProfile() async throws {
        guard let bnetMembershipId = bnetMembershipId else {
            throw BungieApiError.membershipIdMissing
        }
        if (membershipType == nil) {
            guard let linkedProfilesUrl = URL(string: "Destiny2/\(BungieMembershipType.BungieNext.rawValue)/Profile/\(bnetMembershipId)/LinkedProfiles/", relativeTo: self.API_BASE_URL) else {
                throw BungieApiError.urlBuildFailed
            }
            let resp: DestinyLinkedProfilesResponse = try await authApiGet(endpoint: linkedProfilesUrl)
            let profiles = resp.profiles
            let recentProfiles = try profiles.sorted {
                let formatter = DateFormatter()
                guard let first = formatter.date(from: $0.dateLastPlayed) else {
                    throw ParsingError.dateCannotParse
                }
                guard let second = formatter.date(from: $1.dateLastPlayed) else {
                    throw ParsingError.dateCannotParse
                }
                return first.compare(second) == .orderedDescending
            }
            guard let firstProfile = recentProfiles.first else {
                throw BungieApiError.noProfiles
            }
            membershipId = firstProfile.membershipId
            membershipType = firstProfile.membershipType
        }
    }
    
    func getVendors(characterId: String) async throws -> DestinyVendorsResponse {
        guard let membershipType = membershipType?.rawValue else {
            throw BungieApiError.membershipTypeMissing
        }
        guard let membershipId = membershipId else {
            throw BungieApiError.membershipIdMissing
        }
        guard let url = URL(string: "Destiny2/\(membershipType)/Profile/\(membershipId)/Character/\(characterId)/Vendors/", relativeTo: self.API_BASE_URL)?.appending(queryItems: [URLQueryItem(name: "components", value: [DestinyComponentType.Vendors, DestinyComponentType.VendorCategories, DestinyComponentType.VendorSales, DestinyComponentType.CurrencyLookups, DestinyComponentType.StringVariables, DestinyComponentType.ItemInstances, DestinyComponentType.ItemRenderData, DestinyComponentType.ItemStats, DestinyComponentType.ItemSockets, DestinyComponentType.ItemReusablePlugs, DestinyComponentType.ItemTalentGrids, DestinyComponentType.ItemPlugStates, DestinyComponentType.ItemObjectives, DestinyComponentType.ItemPerks].map { String($0.rawValue) }.joined(separator: ","))]) else {
            throw BungieApiError.urlBuildFailed
        }
        let resp: DestinyVendorsResponse = try await self.authApiGet(endpoint: url)
        return resp
    }
    
    func getChars() async throws -> [DestinyCharacterComponent] {
        guard let membershipType = membershipType?.rawValue else {
            throw BungieApiError.membershipTypeMissing
        }
        guard let membershipId = membershipId else {
            throw BungieApiError.membershipIdMissing
        }

        guard let url = URL(string: "Destiny2/\(membershipType)/Profile/\(membershipId)/", relativeTo: self.API_BASE_URL)?.appending(queryItems: [URLQueryItem(name: "components", value: [DestinyComponentType.Characters].map { String($0.rawValue) }.joined(separator: ","))]) else {
            throw BungieApiError.urlBuildFailed
        }
        
        let resp: DestinyProfileResponse = try await self.authApiGet(endpoint: url)
        let chars = Array(resp.characters.data.values)
        return chars
    }
}

enum BungieApiError: Error {
    case refreshWithoutRefreshToken
    case refreshTokenExpired
    case authTokenMissing
    case urlBuildFailed
    case paramMissingFromResp
    case membershipIdMissing
    case membershipTypeMissing
    case manifestMissing
    case noProfiles
    case noCharacters
    case bungieApiError(errorMsg: String)
}

enum ParsingError: Error {
    case dateCannotParse
}

struct AuthCodes: Codable {
    var token: String
    var expires: Date
}

struct BungieTokenResp: Codable {
    let access_token: String
    let token_type: String
    let expires_in: Int
    let refresh_token: String
    let refresh_expires_in: Int
    let membership_id: String
}

protocol DestinyResponse: Codable {}

struct BungieResp<T: DestinyResponse>: Codable {
    let Response: T?
    let ErrorCode: Int
    let ThrottleSeconds: Int
    let ErrorStatus: String
    let Message: String
    let MessageData: [String: String]
    let DetailedErrorTrace: String?
}

enum BungieMembershipType: Int, Codable {
    case None = 0
    case Xbox = 1
    case Psn = 2
    case Steam = 3
    case Blizzard = 4
    case Stadia = 5
    case Egs = 6
    case Demon = 10
    case BungieNext = 254
    case All = -1
}

enum DestinyComponentType: Int, Codable {
    case None = 0
    case Profiles = 100
    case VendorReciepts = 101
    case ProfileInventories = 102
    case ProfileCurrencies = 103
    case ProfileProgression = 104
    case PlatformSilver = 105
    case Characters = 200
    case CharacterInventories = 201
    case CharacterProgressions = 202
    case CharacterRenderData = 203
    case CharacterActivities = 204
    case CharacterEquipment = 205
    case CharacterLoadouts = 206
    case ItemInstances = 300
    case ItemObjectives = 301
    case ItemPerks = 302
    case ItemRenderData = 303
    case ItemStats = 304
    case ItemSockets = 305
    case ItemTalentGrids = 306
    case ItemCommonData = 307
    case ItemPlugStates = 308
    case ItemPlugObjectives = 309
    case ItemReusablePlugs = 310
    case Vendors = 400
    case VendorCategories = 401
    case VendorSales = 402
    case Kiosks = 500
    case CurrencyLookups = 600
    case PresentationNodes = 700
    case Collectibles = 800
    case Records = 900
    case Transitory = 1000
    case Metrics = 1100
    case StringVariables = 1200
    case Craftables = 1300
    case SocialCommendations = 1400
}

struct DictionaryComponentResponse<T: Codable>: Codable {
    let data: [Int: T]
    let privacy: Int
    let disabled: Bool?
}

struct SingleComponentResponse<T: Codable>: Codable {
    let data: T
    let privacy: Int
    let disabled: Bool?
}

struct DestinyManifest: DestinyResponse {
    let version: String
//    let mobileAssetContentPath: String
//    let mobileGearAssetDataBases:
//    let mobileWorldContentPaths: [String: String]
    let jsonWorldContentPaths: [String: String]
//    let jsonWorldComponentContentPaths: [String: ]
//    let mobileClanBannerDatabasePath: String
//    let mobileGearCDN: [String: String]
//    let iconImagePyramidInfo:
}


struct DestinyVendorsResponse: DestinyResponse {
    let vendorGroups: SingleComponentResponse<DestinyVendorGroupComponent>
    let vendors: DictionaryComponentResponse<DestinyVendorComponent>
    let categories: DictionaryComponentResponse<DestinyVendorCategoriesComponent>
    let sales: DictionaryComponentResponse<PersonalDestinyVendorSaleItemSetComponent>
    let itemComponents: [Int: DestinyItemComponentSet?]
    let currencyLookups: SingleComponentResponse<DestinyCurrenciesComponent>
    let stringVariables: SingleComponentResponse<DestinyStringVariablesComponent>
}

struct DestinyVendorGroupComponent: Codable {
    let groups: [DestinyVendorGroup]
}

struct DestinyVendorGroup: Codable {
    let vendorGroupHash: Int
    let vendorHashes: [Int]
}

struct DestinyVendorComponent: Codable {
    let canPurchase: Bool
//    let progression:
    let vendorLocationIndex: Int
    let seasonalRank: Int?
    let vendorHash: Int
    let nextRefreshDate: String
    let enabled: Bool
}

struct DestinyVendorCategoriesComponent: Codable {
    let categories: [DestinyVendorCategory]
}

struct DestinyVendorCategory: Codable {
    let displayCategoryIndex: Int
    let itemIndexes: [Int]
}

struct PersonalDestinyVendorSaleItemSetComponent: Codable {
    let saleItems: [Int: DestinyVendorSaleItemComponent]
}

struct DestinyVendorSaleItemComponent: Codable {
    let saleStatus: Int
    let requiredUnlocks: [Int]?
//    let unlockStatuses:
    let failureIndexes: [Int]
    let augments: Int
    let itemValueVisibility: [Bool]?
    let vendorItemIndex: Int
    let itemHash: Int
    let overrideStyleItemHash: Int?
    let quantity: Int
//    let costs:
    let overrideNextRefreshDate: String?
    let apiPurchasable: Bool?
}

struct DestinyItemComponentSet: Codable {
    let instances: DictionaryComponentResponse<DestinyItemInstanceComponent>
    let stats: DictionaryComponentResponse<DestinyItemStatsComponent>
    let sockets: DictionaryComponentResponse<DestinyItemSocketsComponent>
    let reusablePlugs: DictionaryComponentResponse<DestinyItemReusablePlugsComponent>
    let plugObjectives: DictionaryComponentResponse<DestinyItemPlugObjectivesComponent>?
    let talentGrids: DictionaryComponentResponse<DestinyItemTalentGridComponent>
    let plugStates: DictionaryComponentResponse<DestinyItemPlugComponent>
    let objectives: DictionaryComponentResponse<DestinyItemObjectivesComponent>
    let perks: DictionaryComponentResponse<DestinyItemPerksComponent>
    
    func getItem(vendorItemIndex: Int) -> DestinyItemComponent {
        return DestinyItemComponent(instances: instances.data[vendorItemIndex], stats: stats.data[vendorItemIndex], sockets: sockets.data[vendorItemIndex], reusablePlugs: reusablePlugs.data[vendorItemIndex], plugObjectives: plugObjectives?.data[vendorItemIndex], talentGrids: talentGrids.data[vendorItemIndex], plugStates: plugStates.data[vendorItemIndex], objectives: objectives.data[vendorItemIndex], perks: perks.data[vendorItemIndex])
    }
}

struct DestinyItemComponent {
    let instances: DestinyItemInstanceComponent?
    let stats: DestinyItemStatsComponent?
    let sockets: DestinyItemSocketsComponent?
    let reusablePlugs: DestinyItemReusablePlugsComponent?
    let plugObjectives: DestinyItemPlugObjectivesComponent?
    let talentGrids: DestinyItemTalentGridComponent?
    let plugStates: DestinyItemPlugComponent?
    let objectives: DestinyItemObjectivesComponent?
    let perks: DestinyItemPerksComponent?
}

struct DestinyItemInstanceComponent: Codable {
    let damageType: Int
    let damageTypeHash: Int?
//    let primaryStat:
    let itemLevel: Int
    let quality: Int
    let isEquipped: Bool
    let canEquip: Bool
    let equipRequiredLevel: Int
    let unlockHashesRequiredToEquip: [Int]
    let cannotEquipReason: Int
    let breakerType: DestinyUnlockDefinition?
    let breakerTypeHash: Int?
//    let energy:
}

enum DestinyUnlockDefinition: Int, Codable {
    case None = 0
    case ShieldPiercing = 1
    case Disruption = 2
    case Stagger = 3
}

struct DestinyItemStatsComponent: Codable {
    let stats: [Int: DestinyStat]
}

struct DestinyStat: Codable {
    let statHash: Int
    let value: Int
}

struct DestinyItemSocketsComponent: Codable {
    let sockets: [DestinyItemSocketState]
}

struct DestinyItemSocketState: Codable {
    let plugHash: Int?
    let isEnabled: Bool
    let isVisible: Bool
    let enableFailIndexes: [Int]?
}

struct DestinyItemReusablePlugsComponent: Codable {
    let plugs: [Int: [DestinyItemPlugBase]]
}

struct DestinyItemPlugBase: Codable {
    let plugItemHash: Int
    let canInsert: Bool
    let enabled: Bool
    let insertFailIndexes: [Int]?
    let enableFailIndexes: [Int]?
}

struct DestinyItemPlugObjectivesComponent: Codable {
    let objectivesPerPlug: [Int: [DestinyInventoryItemDefinition]]
}

struct DestinyItemTalentGridComponent: Codable {
    let talentGridHash: Int
//    let nodes:
    let isGridComplete: Bool
//    let gridProgression:
}

struct DestinyItemPlugComponent: Codable {
//    let plugObjectives:
    let plugItemHash: Int
    let canInsert: Bool
    let enabled: Bool
    let insertFailIndexes: [Int]?
    let enableFailIndexes: [Int]?
}

struct DestinyItemObjectivesComponent: Codable {
//    let objectives:
//    let flavorObjective:
    let dataCompleted: Int?
}

struct DestinyItemPerksComponent: Codable {
    let perks: [DestinyPerkReference]
}

struct DestinyPerkReference: Codable {
    let perkHash: Int
    let iconPath: String
    let isActive: Bool
    let visible: Bool
}

struct DestinyCurrenciesComponent: Codable {
    let itemQuantities: [Int: Int]
}

struct DestinyStringVariablesComponent: Codable {
    let integerValuesByHash: [Int: Int]
}

struct DestinyLinkedProfilesResponse: DestinyResponse {
    let profiles: [DestinyProfileUserInfoCard]
    let bnetMembership: UserInfoCard
    let profilesWithErrors: [DestinyErrorProfile]
}

struct DestinyProfileUserInfoCard: Codable {
    let dateLastPlayed: String
    let isOverridden: Bool
    let isCrossSavePrimary: Bool
//    let platformSilver:
    let unpairedGameVersions: Int?
    let supplementalDisplayName: String?
    let iconPath: String?
    let crossSaveOverride: Int
    let applicableMembershipTypes: [Int]
    let isPublic: Bool
    let membershipType: BungieMembershipType
    let membershipId: String
    let displayName: String
    let bungieGlobalDisplayName: String
    let bungieGlobalDisplayNameCode: Int?
}

struct UserInfoCard: Codable {
    let supplementalDisplayName: String?
    let iconPath: String?
    let crossSaveOverride: Int
    let applicableMembershipTypes: [Int]?
    let isPublic: Bool
    let membershipType: Int
    let membershipId: String
    let displayName: String
    let bungieGlobalDisplayName: String
    let bungieGlobalDisplayNameCode: Int?
}

struct DestinyErrorProfile: Codable {
    let errorCode: Int
    let infoCard: UserInfoCard?
}

struct DestinyProfileResponse: DestinyResponse {
    let characters: DictionaryComponentResponse<DestinyCharacterComponent>
}

struct DestinyCharacterComponent: Codable {
    let membershipId: String
    let membershipType: BungieMembershipType
    let characterId: String
    let dateLastPlayed: String
    let minutesPlayedThisSession: String
    let minutesPlayedTotal: String
    let light: Int
    let stats: [Int: Int]
    let raceHash: Int
    let genderHash: Int
    let classHash: Int
    let raceType: Int
    let classType: Int
    let genderType: Int
    let emblemPath: String
    let emblemBackgroundPath: String
    let emblemHash: Int
    let emblemColor: DestinyColor
    let levelProgression: DestinyProgression
    let baseCharacterLevel: Int
    let percentToNextLevel: Double
    let titleRecordHash: Int?
}

struct DestinyColor: Codable {
    let red: Int
    let green: Int
    let blue: Int
    let alpha: Int
}

struct DestinyProgression: Codable {
    let progressionHash: Int
    let dailyProgress: Int
    let dailyLimit: Int
    let weeklyProgress: Int
    let weeklyLimit: Int
    let currentProgress: Int
    let level: Int
    let levelCap: Int
    let stepIndex: Int
    let progressToNextLevel: Int
    let nextLevelAt: Int
    let currentResetCount: Int?
    let seasonResets: [DestinyProgressionResetEntry]?
    let rewardItemStates: [Int]?
}

struct DestinyProgressionResetEntry: Codable {
    let season: Int
    let resets: Int
}
