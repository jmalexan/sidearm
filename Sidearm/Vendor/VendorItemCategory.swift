//
//  VendorCategory.swift
//  Sidearm
//
//  Created by Jonathan Alexander on 2/16/24.
//

import SwiftUI

struct VendorItemCategory: View {
    @Environment(Manifest.self) private var manifest: Manifest
    
    let vendorHash: Int
    let category: DestinyVendorCategory
    let vendorsResp: DestinyVendorsResponse
    
    var body: some View {
        VStack {
            if let displayCategory = manifest.manifest.DestinyVendorDefinition[vendorHash]?.displayCategories[category.displayCategoryIndex] {
                HStack {
                    Text(displayCategory.displayProperties.name)
                    Spacer()
                }
                Divider()
            }
            if let vendorDef = manifest.manifest.DestinyVendorDefinition[vendorHash] {
                LazyVGrid(columns: [GridItem(.adaptive(minimum: 60), alignment: .top)]) {
                    ForEach(category.itemIndexes, id: \.hashValue) { item in
                        if let itemComponents = vendorsResp.itemComponents[vendorHash] ?? nil {
                            ItemBox(itemHash: vendorDef.itemList[item].itemHash, itemComponent: itemComponents.getItem(vendorItemIndex: item))
                                .frame(height: 60)
                        }
                    }
                }
            }
        }
    }
}
