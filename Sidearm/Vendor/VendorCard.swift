//
//  VendorCard.swift
//  Sidearm
//
//  Created by Jonathan Alexander on 2/14/24.
//

import SwiftUI

struct VendorCard: View {
    @Environment(Manifest.self) private var manifest: Manifest
    
    let vendorHash: Int
    let vendorsResp: DestinyVendorsResponse
    
    func getBgImg() -> String? {
        if let vendor = vendorsResp.vendors.data[vendorHash] {
            return manifest.manifest.DestinyVendorDefinition[vendorHash]?.locations?[vendor.vendorLocationIndex].backgroundImagePath
        }
        return nil
    }
    
    var body: some View {
        ZStack {
            Color.gray
            if let bgImg = getBgImg() {
                Color.clear.background {
                    AsyncImage(url: URL(string: "https://www.bungie.net\(bgImg)")) { image in
                        image.image?
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .clipped()
                    }
                }
            } else {
                Color.red
            }
            if let display = manifest.manifest.DestinyVendorDefinition[vendorHash]?.displayProperties {
                VStack {
                    Spacer()
                    HStack {
                        VStack {
                            HStack {
                                Text(display.name)
                                //                                        .foregroundStyle(.white)
                                    .font(.title)
                                Spacer()
                            }
                            HStack {
                                Text(display.subtitle)
                                //                                        .foregroundStyle(UIVibrancy)
                                Spacer()
                            }
                        }
                        Spacer()
                    }
                    .padding()
                    .background(.regularMaterial)
                }
            }
        }
        .frame(height: 250)
        .clipped()
    }
}
