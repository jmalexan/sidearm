//
//  VendorView.swift
//  Sidearm
//
//  Created by Jonathan Alexander on 2/13/24.
//

import SwiftUI

struct AllVendorView: View {
    @Environment(\.horizontalSizeClass) var horizontalSizeClass
    @Environment(BungieApi.self) private var api: BungieApi
    @Environment(Manifest.self) private var manifest: Manifest
    @State private var vendorsData: DestinyVendorsResponse?
    @State private var chars: [DestinyCharacterComponent]?
    @State private var selectedChar: String?
    
    var content: some View {
        VStack {
            if let vendorsData = vendorsData {
                ScrollView {
                    LazyVStack {
                        ForEach(vendorsData.vendorGroups.data.groups, id: \.vendorGroupHash) {group in
                            VendorGroup(vendorsData: vendorsData, group: group)
                                .padding(.vertical)
                        }
                    }
                }
            } else {
                ProgressView().padding()
            }
        }
        .toolbar {
            if let chars = chars {
                ToolbarItem {
                    Menu {
                        ForEach(chars, id: \.characterId) { char in
                            Button {
                                Task { try await setChar(char: char.characterId) }
                            } label: {
                                Label(char.characterId, systemImage: "person")
                            }
                        }
                    } label: {
                        Image(systemName: "person")
                    }
                }
            }
        }
        .onAppear() {
            Task {
                do {
                    let newChars = try await api.getChars()
                    chars = newChars
                    guard let firstChar = newChars.first else {
                        throw BungieApiError.noCharacters
                    }
                    try await setChar(char: firstChar.characterId)
                } catch {
                    print(error)
                }
            }
        }
        .navigationTitle("Vendors")
    }
    
    var body: some View {
        if horizontalSizeClass == .compact {
            content
        } else {
            NavigationStack {
                content
            }
        }
    }
    
    func setChar(char: String) async throws {
        vendorsData = nil
        selectedChar = char
        vendorsData = try await api.getVendors(characterId: char)
    }
}
