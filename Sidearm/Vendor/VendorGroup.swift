//
//  VendorGroup.swift
//  Sidearm
//
//  Created by Jonathan Alexander on 2/15/24.
//

import SwiftUI

struct VendorGroup: View {
    @Environment(Manifest.self) private var manifest: Manifest
    let vendorsData: DestinyVendorsResponse
    let group: DestinyVendorGroup
    
    var body: some View {
        VStack {
            if let groupDef = manifest.manifest.DestinyVendorGroupDefinition[group.vendorGroupHash] {
                VStack {
                    Text(groupDef.categoryName)
                    Divider()
                }
            } else {
                Text("Error")
            }
            LazyVGrid(columns: [GridItem(.adaptive(minimum: 400), alignment: .top)]) {
                ForEach(group.vendorHashes, id: \.self) {vendorHash in
                    NavigationLink {
                        VendorView(vendorHash: vendorHash, vendorsResp: vendorsData)
                    } label: {
                        VendorCard(vendorHash: vendorHash, vendorsResp: vendorsData)
                            .hoverEffect()
                            .clipShape(RoundedRectangle(cornerRadius: 16))
                            .padding()
                            .shadow(radius: 10)
                    }
                    .buttonStyle(.plain)
                }
            }
        }
    }
}
