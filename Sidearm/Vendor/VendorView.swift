//
//  VendorView.swift
//  Sidearm
//
//  Created by Jonathan Alexander on 2/16/24.
//

import SwiftUI

struct VendorView: View {
    @Environment(Manifest.self) private var manifest: Manifest
    
    let vendorHash: Int
    let vendorsResp: DestinyVendorsResponse
    
    @State var showInfoSheet = false
    
    func getInfoText() -> String? {
        guard let vendorDef = manifest.manifest.DestinyVendorDefinition[vendorHash] else {
            return nil
        }
        guard let infoCategory = vendorsResp.categories.data[vendorHash]?.categories.first(where: { vendorDef.displayCategories[$0.displayCategoryIndex].identifier == "tooltip.help.name" }) else {
            return nil
        }
        guard let itemDef = manifest.manifest.DestinyInventoryItemDefinition[vendorDef.itemList[infoCategory.itemIndexes[0]].itemHash] else {
            return nil
        }
        return itemDef.displayProperties.description
    }
    
    var body: some View {
        ScrollView {
            VendorCard(vendorHash: vendorHash, vendorsResp: vendorsResp)
            if let categories = vendorsResp.categories.data[vendorHash]?.categories{
                VStack {
                    ForEach(categories.sorted(by: { $0.displayCategoryIndex < $1.displayCategoryIndex } ).filter({ !$0.itemIndexes.isEmpty && manifest.manifest.DestinyVendorDefinition[vendorHash]?.displayCategories[$0.displayCategoryIndex].identifier != "tooltip.help.name" }), id: \.displayCategoryIndex) {category in
                        VendorItemCategory(vendorHash: vendorHash, category: category, vendorsResp: vendorsResp)
                            .padding()
                    }
                }
            }
        }
        .toolbar {
            if let infoText = getInfoText() {
                Button {
                    showInfoSheet.toggle()
                } label: {
                    Image(systemName: "info.circle")
                }
                .sheet(isPresented: $showInfoSheet) {
                    NavigationStack {
                        VStack {
                            Text(infoText)
                            Spacer()
                        }
                        .padding()
                        .toolbar {
                            ToolbarItem {
                                Button("Dismiss") {
                                    showInfoSheet.toggle()
                                }
                            }
                        }
                    }
                }
            }
        }
        .navigationTitle(manifest.manifest.DestinyVendorDefinition[vendorHash]?.displayProperties?.name ?? "")
        #if !os(macOS)
            .navigationBarTitleDisplayMode(.inline)
        #endif

    }
}
