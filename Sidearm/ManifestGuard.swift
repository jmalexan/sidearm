//
//  ManifestGuard.swift
//  Sidearm
//
//  Created by Jonathan Alexander on 2/13/24.
//

import SwiftUI

struct ManifestGuard<Content: View>: View {
    @Environment(BungieApi.self) private var api: BungieApi
    @State private var status: String?
    @State private var error = false
    @State private var manifest: Manifest?
    @ViewBuilder var content: () -> Content
    
    var body: some View {
        VStack {
            if let manifest = manifest {
                content()
                    .environment(manifest)
            } else {
                if (self.error) {
                    ProgressView().padding()
                } else {
                    ProgressView().padding()
                }
                Text(status ?? "")
            }
        }.onAppear {
            self.manifest = nil
            Task {
                do {
                    self.status = "Checking for new manifest..."
                    var manifestData: Data
                    if let (newManifestPath, newManifestVersion) = try await api.checkNewManifest() {
                        self.status = "Downloading manifest..."
                        manifestData = try await api.getNewManifest(path: newManifestPath, version: newManifestVersion)
                    } else {
                        if let manifestCache = api.getManifestCache() {
                            manifestData = manifestCache
                        } else {
                            throw BungieApiError.manifestMissing
                        }
                    }
                    self.status = "Parsing manifest..."
                    let newManifest = try JSONDecoder().decode(ManifestTables.self, from: manifestData)
                    self.manifest = Manifest(manifest: newManifest)
                    self.status = ""
                } catch {
                    print(error)
                    self.error = true
                    self.status = error.localizedDescription
                }
            }
        }
    }
}
