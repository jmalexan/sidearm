//
//  SidearmApp.swift
//  Sidearm
//
//  Created by Jonathan Alexander on 2/12/24.
//

import SwiftUI
import SwiftData

@main
struct SidearmApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .modelContainer(for: BungieApi.self)
        }
    }
}
