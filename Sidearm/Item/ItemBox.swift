//
//  ItemBox.swift
//  Sidearm
//
//  Created by Jonathan Alexander on 2/16/24.
//

import SwiftUI

struct ItemBox: View {
    @Environment(Manifest.self) private var manifest: Manifest
    
    let itemHash: Int
    let itemComponent: DestinyItemComponent
    
    @State private var isShowingSheet = false
    
    var body: some View {
        Button(action: {
            isShowingSheet.toggle()
        }) {
            if let itemIconPath = manifest.manifest.DestinyInventoryItemDefinition[itemHash]?.displayProperties.icon {
                AsyncImage(url: URL(string: "https://www.bungie.net\(itemIconPath)")) { image in
                    image.image?
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                }
            }
        }
        .buttonStyle(.plain)
        .sheet(isPresented: $isShowingSheet) {
            NavigationStack {
                VStack {
                    if let screenshot = manifest.manifest.DestinyInventoryItemDefinition[itemHash]?.screenshot {
                        AsyncImage(url: URL(string: "https://www.bungie.net\(screenshot)")) { image in
                            image.image?
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                        }
                        .frame(height: 250)
                        .clipped()
                    }
                    VStack {
                        if let displayProperties = manifest.manifest.DestinyInventoryItemDefinition[itemHash]?.displayProperties {
                            HStack {
                                Text(displayProperties.name)
                                    .font(.title)
                                Spacer()
                            }
                        }
                        if let flavorText = manifest.manifest.DestinyInventoryItemDefinition[itemHash]?.flavorText {
                            HStack {
                                Text(flavorText)
                                Spacer()
                            }
                        }
                    }
                    .padding()
                    Spacer()
                }
                .toolbar {
                    ToolbarItem {
                        Button("Dismiss") {
                            isShowingSheet.toggle()
                        }
                    }
                }
            }
        }
    }
}

/**
 A NavigationLink is presenting a NavigationStack onto a column. This construction is not supported. Consider instead just presenting a new root view which will replace that of an existing stack in the column, or else the column itself if there is no NavigationStack in the target column. If there is a stack in the target column, appending to that stack's path is another option.
 */
