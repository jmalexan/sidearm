//
//  RootView.swift
//  Sidearm
//
//  Created by Jonathan Alexander on 2/13/24.
//

import SwiftUI

struct RootView: View {
    @Environment(\.horizontalSizeClass) var horizontalSizeClass
    @Environment(BungieApi.self) private var api: BungieApi
    
    var content: some View {
        VStack {
            List {
                NavigationLink {
                    InventoryView()
                } label: {
                    Label(
                        title: { Text("Inventory") },
                        icon: { Image(systemName: "backpack") }
                    )
                }
                NavigationLink {
                    AllVendorView()
                } label: {
                    Label(
                        title: { Text("Vendors") },
                        icon: { Image(systemName: "person") }
                    )
                }
//                    Button("Clear manifest") {
//                        do {
//                            try api.setNewManifest(data: Data(), newManifestVersion: "")
//                        } catch {
//                            print(error)
//                        }
//                    }
            }.navigationTitle("Sidearm")
        }
        .onAppear() {
            Task {
                do {
                    try await api.checkProfile()
                } catch {
                    print(error)
                }
            }
        }
    }
        
    var body: some View {
        if horizontalSizeClass == .compact {
            NavigationStack {
                content
            }
        } else {
            NavigationSplitView {
                content
            } detail: {
                Text("Welcome to Sidearm!")
            }
        }
    }
}
