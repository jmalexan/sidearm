//
//  ContentView.swift
//  Sidearm
//
//  Created by Jonathan Alexander on 2/12/24.
//

import SwiftUI
import SwiftData

struct ContentView: View {
    @Environment(\.modelContext) private var context
    @Query private var apis: [BungieApi]
    @State private var api: BungieApi?
        
    var body: some View {
        VStack {
            if (self.api != nil) {
                AuthGuard {
                    ManifestGuard {
                        RootView()
                    }
                }
                .environment(api)
            } else {
                ProgressView().padding()
            }
        }.onAppear() {
            if let api = self.apis.first {
                self.api = api
            } else {
                let newApi = BungieApi()
                self.api = newApi
                context.insert(newApi)
            }
        }
    }
}
