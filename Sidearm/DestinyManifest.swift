//
//  DestinyManifest.swift
//  Sidearm
//
//  Created by Jonathan Alexander on 2/13/24.
//

import Foundation

@Observable
class Manifest: Codable {
    var manifest: ManifestTables
    
    init(manifest: ManifestTables) {
        self.manifest = manifest
    }
}

struct ManifestTables: Codable {
    var DestinyInventoryItemDefinition: [Int: DestinyInventoryItemDefinition]
    var DestinyVendorDefinition: [Int: DestinyVendorDefinition]
    var DestinyVendorGroupDefinition: [Int: DestinyVendorGroupDefinition]
}

struct DestinyInventoryItemDefinition: Codable {
    let displayProperties: DestinyDisplayPropertiesDefinition
//    let tooltipNotifications:
    let collectibleHash: Int?
    let iconWatermark: String?
    let iconWatermarkShelved: String?
    let secondaryIcon: String?
    let secondaryOverlay: String?
    let secondarySpecial: String?
//    let backgroundColor:
    let screenshot: String?
    let itemTypeDisplayName: String?
    let flavorText: String?
    let uiItemDisplayStyle: String?
    let itemTypeAndTierDisplayName: String?
    let displaySource: String?
    let tooltipStyle: String?
//    let action:
//    let crafting:
//    let inventory:
//    let setData:
//    let stats:
    let emblemObjectiveHash: Int?
//    let equippingBlock:
//    let translationBlock:
//    let preview:
//    let quality:
//    let value:
//    let sourceData:
//    let objectives:
//    let metrics:
//    let plug:
//    let gearset:
//    let sack:
//    let sockets:
//    let summary:
//    let talentGrid:
//    let investmentStats:
//    let perks:
    let loreHash: Int?
    let summaryItemHash: Int?
//    let animations:
    let allowActions: Bool
//    let links:
    let doesPostmasterPullHaveSideEffects: Bool
    let nonTransferrable: Bool
    let itemCategoryHashes: [Int]?
    let specialItemType: Int
    let itemType: Int
    let itemSubType: Int
    let classType: Int
    let breakerType: Int
    let breakerTypeHash: Int?
    let equippable: Bool
    let damageTypeHashes: [Int]?
    let damageTypes: [Int]?
    let defaultDamageType: Int
    let defaultDamageTypeHash: Int?
    let seasonHash: Int?
    let isWrapper: Bool
    let traitIds: [String]?
    let traitHashes: [Int]?
    let hash: Int
    let index: Int
    let redacted: Bool
}

struct DestinyDisplayPropertiesDefinition: Codable {
    let description: String
    let name: String
    let icon: String?
    let highResIcon: String?
    let hasIcon: Bool
}

struct DestinyVendorDefinition: Codable {
    let displayProperties: DestinyVendorDisplayPropertiesDefinition?
    let vendorProgressionType: Int
    let buyString: String?
    let sellString: String?
    let displayItemHash: Int
    let inhibitBuying: Bool
    let inhibitSelling: Bool
    let factionHash: Int
    let resetIntervalMinutes: Int
    let resetOffsetMinutes: Int
    let failureStrings: [String]
//    let unlockRanges:
    let vendorIdentifier: String?
    let vendorPortrait: String?
    let vendorBanner: String?
    let enabled: Bool
    let visible: Bool
    let vendorSubcategoryIdentifier: String?
    let consolidateCategories: Bool
//    let actions: []
    let categories: [DestinyVendorCategoryEntryDefinition]
    let originalCategories: [DestinyVendorCategoryEntryDefinition]
    let displayCategories: [DestinyDisplayCategoryDefinition]
//    let interactions: []
//    let inventoryFlyouts: []
    let itemList: [DestinyVendorItemDefinition]
//    let services: []
//    let acceptedItems: []
    let returnWithVendorRequest: Bool
    let locations: [DestinyVendorLocationDefinition]?
//    let groups: []
    let ignoreSaleItemHashes: [Int]
    let hash: Int
    let index: Int
    let redacted: Bool
}

struct DestinyVendorDisplayPropertiesDefinition: Codable {
    let largeIcon: String?
    let subtitle: String
    let originalIcon: String?
    let smallTransparentIcon: String?
    let mapIcon: String?
    let largeTransparentIcon: String?
    let description: String
    let name: String
    let icon: String?
    let highResIcon: String?
    let hasIcon: Bool
}

struct DestinyVendorLocationDefinition: Codable {
    let destinationHash: Int
    let backgroundImagePath: String?
}

struct DestinyVendorCategoryEntryDefinition: Codable {
    let categoryIndex: Int
    let sortValue: Int
    let categoryHash: Int
    let quantityAvailable: Int
    let showUnavailableItems: Bool
    let hideIfNoCurrency: Bool
    let hideFromRegularPurchase: Bool
    let buyStringOverride: String
    let disabledDescription: String
    let displayTitle: String?
//    let overlay:
    let vendorItemIndexes: [Int]
    let isPreview: Bool
    let isDisplayOnly: Bool
    let resetIntervalMinutesOverride: Int
    let resetOffsetMinutesOverride: Int
}

struct DestinyDisplayCategoryDefinition: Codable {
    let index: Int
    let identifier: String
    let displayCategoryHash: Int
    let displayProperties: DestinyDisplayPropertiesDefinition
    let displayInBanner: Bool
    let progressionHash: Int?
    let sortOrder: Int
    let displayStyleHash: Int?
    let displayStyleIdentifier: String?
}

struct DestinyVendorItemDefinition: Codable {
    let vendorItemIndex: Int
    let itemHash: Int
    let quantity: Int
    let failureIndexes: [Int]
//    let currencies: []
    let refundPolicy: Int
    let refundTimeLimit: Int
//    let creationLevels: []
    let displayCategoryIndex: Int
    let categoryIndex: Int
    let originalCategoryIndex: Int
    let minimumLevel: Int
    let maximumLevel: Int
//    let action:
    let displayCategory: String
    let inventoryBucketHash: Int
    let visibilityScope: Int
    let purchasableScope: Int
    let exclusivity: Int
    let isOffer: Bool?
    let isCrm: Bool?
    let sortValue: Int
    let expirationTooltip: String
    let redirectToSaleIndexes: [Int]
//    let socketOverrides: []
    let unpurchasable: Bool?
}

struct DestinyVendorGroupDefinition: Codable {
    let order: Int
    let categoryName: String
    let hash: Int
    let index: Int
    let redacted: Bool
}
