//
//  AuthGuard.swift
//  Sidearm
//
//  Created by Jonathan Alexander on 2/13/24.
//

import SwiftUI

struct AuthGuard<Content: View>: View {
    @Environment(BungieApi.self) private var api: BungieApi
    @State private var handlingRedirect = false
    @ViewBuilder var content: () -> Content
    
    var body: some View {
        VStack {
            if (self.api.isLoggedIn()) {
                content()
            } else {
                if (self.handlingRedirect) {
                    ProgressView().padding()
                    Text("Logging in...")
                } else {
                    VStack {
                        Link("Login", destination: self.api.getLoginUrl())
                    }.padding()
                }
            }
        }.onOpenURL(perform: { url in
            guard url.scheme == "d2sidearm" else {
                return
            }
            
            guard let components = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
                print("Invalid URL")
                return
            }
            
            guard let action = components.host, action == "redirect" else {
                print("Unknown URL, we can't handle this one!")
                return
            }
            
            guard let code = components.queryItems?.first(where: { $0.name == "code" })?.value else {
                print("Code not found")
                return
            }
            
            Task {
                self.handlingRedirect = true
                await self.api.tokenLogin(code: code)
                self.handlingRedirect = false
            }
        })
    }
}
